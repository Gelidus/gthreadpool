(C++11)Welcome to GCallBackLib - written by Gelidus
===================================================

***
What is GCallBackLib ?
----------------------
***
GCallBackLib is a C++11 lib designed to work with threads depending on needs of applications at runtime.
System is based on so-called CallBacks which are called by system threads by their priority.
The current version of System is : 0.2.1 APLHA
***
How to use it?
--------------
***
One of main goals was to make the system easy to use and implement. Here is the sample : 

```C++
using namespace std;
using namespace GCallBackLib; // for better manipulating in code

void DoSomething(void* arg, GWorkingThread *exThread) { std::cout << "Executed" << std::endl; } // function used as a Callback

int main(int argc, char *argv[]) {
    GCallBackMgr *instance = new GCallBackMgr();  // creating new instance of callback manager
    
    // initializing instance, first parameter is number of permanent threads
    // second parameter is number of maximum threads - as they are created at runtime
    // third parameter is number of miliseconds the threads will last without work
	instance->Init(1,15,3000);
    
    // creating new callback instance with DoSomething as a callback, instance as a call manager and 
    // low priority - if there would be more callbacks, the ones with higher priority
    // will every time be called first
	GCallBack *callback = new GCallBack(DoSomething, instance, GCallBack::PRIORITY::LOW);
    
    // and giving it try ... push 100 callbacks to system and it will execute it, Asynchronously!
	for(auto i = 0; i < 100; i++) {
		callback->ASync(NULL); // one void * parameter can be pushed into the callback
	}
    
    //let's say we want to wait for callbacks to be executed and temporary threads to be stopped
    int i; cin >> i;
    
    //and now, we just force system to be stopped - no more callbacks will be executed and no threads are running
	instance->ForceStop();
    
    // doing cleanum after system use
	delete callback; callback = 0;
	delete instance; instance = 0;
	return 0;
}
```
***
Who am I ?
----------
***
I'm 19 years old developer from Slovakia. You can see my skills listed in my portfolio below.
Feel free to contact me here or via email.
***
List of links : 
***
[Portfolio](http://gelidus.koding.com)
***
[LinkedIn](http://www.linkedin.com/pub/peter-malina/32/30a/54b)
***
[Facebook](http://www.facebook.com/Gelidus5)
***