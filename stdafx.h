/*********************************************
 *	@author - Gelidus (Peter Malina)
 *
 *	Copyright (c) - Gelidus (Peter Malina)
 *	This system can be used in non-profit
 *	applications or for educational purpose 
 *	without contacting author.
 *********************************************/

#pragma once

// It's just not needed for system compilation
//#include "targetver.h"
//#include <tchar.h>

#include <stdio.h>
#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <thread>
#include <time.h>
#include <mutex>
#include <algorithm>

#include "GCallBackMgr.h"