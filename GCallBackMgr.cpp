/*********************************************
 *	@author - Gelidus (Peter Malina)
 *
 *	Copyright (c) - Gelidus (Peter Malina)
 *	This system can be used in non-profit
 *	applications or for educational purpose 
 *	without contacting author.
 *********************************************/

#include "stdafx.h"

namespace GCallBackLib { /* GLOBAL FUNCTIONS */
	void ClearObject(void *object) {
		delete object;
		object = 0;
	}

	void ClearObject(void *objects[]) {
		int length = (sizeof(objects) / sizeof(objects[0]));

		for(int i = 0; i < length; i++) {
			ClearObject(objects[i]);
		}
	}
};

namespace GCallBackLib {
	GCallBackMgr::~GCallBackMgr() {
	}

	void GCallBackMgr::SetThreadExecutingCallBack(bool exec) { 
		SetExecutingCallBackThreadsMutex.lock();
		if(exec) {
			this->currentlyCallBackExecutingThreads++;
		} else {
			this->currentlyCallBackExecutingThreads--;
		}
		SetExecutingCallBackThreadsMutex.unlock();
	}

	void GCallBackMgr::SetThreadWorking(bool working) { 
		SetWorkingThreadsMutex.lock();
		if(working) { 
			currentlyWorkingThreads++;
		} else {
			currentlyWorkingThreads--;
		} 
		SetWorkingThreadsMutex.unlock();
	}

	void GCallBackMgr::Init(unsigned int permaThreads = 1, unsigned int maxThreads = 1, unsigned int timeout = 30000) {
		if(permaThreads < 1) {
			permaThreads = 1;
		} 
		if(maxThreads < 1) {
			maxThreads = 1;
		}

		this->permaThreads = 0; // handled in AddThread and remove
		this->maxThreads = maxThreads;
		this->threadTimeout = timeout;
		this->currentlyWorkingThreads = 0;
		this->currentlyCallBackExecutingThreads = 0;
		this->pivot = DEFAULT_PIVOT;

		for(unsigned int i = 0; i < permaThreads; i++) {
			AddThread(true);
		}
	}

	// never ever touch this again
	void GCallBackMgr::ForceStop() {
		WorkingThreadsMutex.lock();
		if(!WorkingThreads.empty()) {
			for(WorkingThreadsContainer::iterator it = WorkingThreads.begin(); it != WorkingThreads.end(); it++) {
				if(!it->second->IsPermanent()) {
					it->second->Stop();
				}
			}
			WorkingThreadsMutex.unlock();

			while(WorkingThreads.size() - permaThreads != 0);

			WorkingThreadsMutex.lock();

			for(WorkingThreadsContainer::iterator it = WorkingThreads.begin(); it != WorkingThreads.end(); it++) {
				it->second->Stop();
				it->second->Join();
				ClearObject(it->second);
			}

			WorkingThreads.clear();
		}
		WorkingThreadsMutex.unlock();
	}

	unsigned int GCallBackMgr::GenerateId() {
		unsigned int next = 0;
		for(;;next++) {
			if(WorkingThreads.find(next) == WorkingThreads.end()) {
				return next;
			}
		}
	}

	void GCallBackMgr::HandleThreadStop(unsigned int id) {
		WorkingThreadsContainer::iterator it = WorkingThreads.find(id);
		if(it == WorkingThreads.end()) {
			return;
		} else {
			RemoveThreadStruct *remover = new RemoveThreadStruct(it->first, this);
			GCallBack *cb = new GCallBack(RemoveThread, this, GCallBack::PRIORITY::NONE);
			cb->ASync((void*)(remover));
		}
	}

	void GCallBackMgr::AddThread(bool perma = false) {
		unsigned id = this->GenerateId();
		GWorkingThread *thread = new GWorkingThread(id, this->threadTimeout, this, perma);

		this->WorkingThreadsMutex.lock();

		this->WorkingThreads[id] = thread;
		if(perma) {
			this->permaThreads++;
		}
		this->WorkingThreadsMutex.unlock();

		thread->Start();
	}

	void GCallBackMgr::RemoveThread(void *structure, GWorkingThread *thread) {
		RemoveThreadStruct *rstruct = static_cast<RemoveThreadStruct*>(structure);

		if(rstruct->id == thread->GetId()) { // HACKYFIX - will fix this later
			RemoveThreadStruct *remover = new RemoveThreadStruct(rstruct->id, rstruct->manager);
			GCallBack *cb = new GCallBack(RemoveThread, rstruct->manager, GCallBack::PRIORITY::NONE);
			ClearObject(rstruct);
			cb->ASync((void*)(remover));
			return;
		}

		rstruct->manager->LockWorkingThreadsMutex();
		WorkingThreadsContainer::iterator it = rstruct->manager->GetWorkingThreadIteratorById(rstruct->id);

		it->second->Join();
		ClearObject(it->second);
		rstruct->manager->EraseThreadOnIterator(it);

		rstruct->manager->UnLockWorkingThreadsMutex();

		ClearObject(rstruct);
	}

	void GCallBackMgr::AddCallBack(GCallBack *callback) {
		CallBacksMutex.lock();
		bool pushed = false;

		for(CallBackContainer::iterator it = CallBacks.begin(); it != CallBacks.end(); it++) {
			if(callback->GetPriority() < (*it)->GetPriority()) {
				CallBacks.insert(it, callback);
				pushed = true;
			}
		}
		if(!pushed) {
			this->CallBacks.push_back(callback);
		}
		CallBacksMutex.unlock();

		if(this->GetWorkingThreadsNum() < this->maxThreads) {
			GCallBack::PRIORITY priority = callback->GetPriority();

			if(priority != GCallBack::PRIORITY::NONE) {
				unsigned int tPivot; // used just in HIGH priority case

				switch(priority) {
					case GCallBack::PRIORITY::VERY_HIGH:
						if(!this->HasFreeThread()) {
							AddThread(false);
						}
						break;

					case GCallBack::PRIORITY::HIGH:
						tPivot = (int)((pivot / 2) + 0.5f);
						if(this->GetNumberOfQueuedCallbacks() >= tPivot) {
							AddThread(false);
						}
						break;

					case GCallBack::PRIORITY::NORMAL:
						if(this->GetNumberOfQueuedCallbacks() > pivot) {
							AddThread(false);
						}
						break;
					case GCallBack::PRIORITY::LOW:
						if(this->GetNumberOfQueuedCallbacks() > 3*pivot) {
							AddThread(false);
						}
						break;
				}
			}
		}
	}	

	GCallBack *GCallBackMgr::RipCallBack() {
		this->CallBacksMutex.lock();

		if(this->CallBacks.empty()) {
			this->CallBacksMutex.unlock();
			return nullptr;
		}
		GCallBack *cb = *CallBacks.begin();
		CallBacks.erase(CallBacks.begin());
		this->CallBacksMutex.unlock();
	
		return cb;
	}

	bool GCallBackMgr::HasFreeThread() {
		return this->currentlyCallBackExecutingThreads < this->GetWorkingThreadsNum() ? true : false;
	}

	void GCallBackMgr::BalancePermaThreads(int balance) {
		if(balance == 0) { // we dont like this
			return;
		}

		WorkingThreadsMutex.lock();

		unsigned int absBalance = abs(balance);
		if(balance < 0 && absBalance > GetPermaThreadsNum()) {
			absBalance = GetPermaThreadsNum() - 1; // one perma thread is needed every time
		}

		for(unsigned int i = 0; i < absBalance; i++) {
			if(balance < 0) {
				for(WorkingThreadsContainer::iterator it = WorkingThreads.begin(); it != WorkingThreads.end() && i < absBalance; it++) {
					if(it->second->IsPermanent()) {
						it->second->Stop();
						HandleThreadStop(it->first);
						this->permaThreads--;
						i++;
					}
				}
			} else {
				AddThread(true);
			}
		}

		WorkingThreadsMutex.unlock();
	}

	void GCallBackMgr::BalanceMaxThreads(int balance) {
		if(balance > 0) {
			this->maxThreads += balance;
		} else {
			unsigned int absBalance = abs(balance);

			if(GetMaxThreadsNum() - absBalance >= GetPermaThreadsNum()) {
				this->maxThreads -= absBalance;
			} else {
				this->maxThreads = 1;
				BalancePermaThreads(-(int)(GetPermaThreadsNum()+1));
			}
		}
	}
};