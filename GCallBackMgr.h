/*********************************************
 *	@author - Gelidus (Peter Malina)
 *
 *	Copyright (c) - Gelidus (Peter Malina)
 *	This system can be used in non-profit
 *	applications or for educational purpose 
 *	without contacting author.
 *********************************************/

#include "GWorkingThread.h"
#include "GCallBack.h"

namespace GCallBackLib
{
#define DEFAULT_PIVOT			5

	//--------------------------------------------------------------
	// Use this method to clear object from app
	//--------------------------------------------------------------
	void ClearObject(void *object);
	void ClearObject(void *objects[]);

	class GCallBackMgr {
	private:
		typedef std::vector<GCallBack*> CallBackContainer;

		typedef std::map<unsigned int, GWorkingThread*> WorkingThreadsContainer;

		struct RemoveThreadStruct {
			RemoveThreadStruct(unsigned int id, GCallBackMgr *mgr) {
				this->id = id;
				this->manager = mgr;
			};

			unsigned int id;
			GCallBackMgr *manager;
		};

	private:
		//--------------------------------------------------------------
		// Specifies how many threads should run permanently
		//--------------------------------------------------------------
		unsigned int permaThreads;

		//--------------------------------------------------------------
		// Specifies the maximum number of all threads running at one time
		//--------------------------------------------------------------
		unsigned int maxThreads;

		//--------------------------------------------------------------
		// Specifies how many threads are working at the moment
		//--------------------------------------------------------------
		unsigned int currentlyWorkingThreads;

		//--------------------------------------------------------------
		// Specifies how many threads executing callback at the moment
		//--------------------------------------------------------------
		unsigned int currentlyCallBackExecutingThreads;

		//--------------------------------------------------------------
		// Specifies the temporary thread timeout
		//--------------------------------------------------------------
		unsigned int threadTimeout;

		//--------------------------------------------------------------
		// Map that stores all working threads in manager
		//--------------------------------------------------------------
		WorkingThreadsContainer WorkingThreads;

		//--------------------------------------------------------------
		// Container of callbacks
		//--------------------------------------------------------------
		CallBackContainer CallBacks;

		//--------------------------------------------------------------
		// Use as a core mechanism for determining need of new threads
		// DEFAULT VALUE ... [5]
		// $5 NONE - no pivot
		// $4 LOW - pivot * 3
		// $3 NORMAL - pivot
		// $4 HIGH - pivot / 2
		// $5 VERY_HIGH - no pivot
		//--------------------------------------------------------------
		unsigned int pivot;

		//--------------------------------------------------------------
		// Mutex locking on thread container manipulation
		//--------------------------------------------------------------
		std::mutex WorkingThreadsMutex;

		//--------------------------------------------------------------
		// Mutex locking on callback container manipulation
		//-------------------------------------------------------------
		std::mutex CallBacksMutex;

		//--------------------------------------------------------------
		// Mutex locking on setting working threads
		//-------------------------------------------------------------
		std::mutex SetWorkingThreadsMutex;

		//--------------------------------------------------------------
		// Mutex locking on setting callback executing threads
		//-------------------------------------------------------------
		std::mutex SetExecutingCallBackThreadsMutex;

	public:
		//--------------------------------------------------------------
		// Manager Destructor
		//--------------------------------------------------------------
		~GCallBackMgr();

		//--------------------------------------------------------------
		// Method used to initialize the manager
		// $permaThreads = DEFAULT 1, MINIMUM 1
		// $maxThreads = DEFAULT 1, MINIMUM 1
		// $timeout = DEFAULT 3000
		//--------------------------------------------------------------
		void Init(unsigned int permaThreads, unsigned int maxThreads, unsigned int timeout);

		//--------------------------------------------------------------
		// Returns the number of all working threads
		//--------------------------------------------------------------
		unsigned int GetWorkingThreadsNum() { return this->currentlyWorkingThreads; }

		//--------------------------------------------------------------
		// Returns true if there is 1 or more threads avaliable in mgr
		//--------------------------------------------------------------
		bool HasFreeThread();

		//--------------------------------------------------------------
		// Getter for number of running perma threads
		//--------------------------------------------------------------
		unsigned int GetPermaThreadsNum() { return this->permaThreads; }

		//--------------------------------------------------------------
		// Method used to balance perma threads, to add 1, set parameter
		// to 1, to remove 1, set to -1
		// Never manipulate with this in more threads at once
		//--------------------------------------------------------------
		void BalancePermaThreads(int balance);

		//--------------------------------------------------------------
		// Method used to balance the number of maximum threads
		//--------------------------------------------------------------
		void BalanceMaxThreads(int balance);

		//--------------------------------------------------------------
		// Getter for number of maximum running threads in manager
		//--------------------------------------------------------------
		unsigned int GetMaxThreadsNum() { return this->maxThreads; }

		//--------------------------------------------------------------
		// Warning! blocking function to current thread - will wait for
		// all working threads to be stopped and then returns
		//--------------------------------------------------------------
		void ForceStop();

		void SetThreadWorking(bool working);

		void SetThreadExecutingCallBack(bool exec);

		void LockWorkingThreadsMutex() { this->WorkingThreadsMutex.lock(); }

		void UnLockWorkingThreadsMutex() { this->WorkingThreadsMutex.unlock(); }

		void AddCallBack(GCallBack*);

		//--------------------------------------------------------------
		// Method called when temporary thread stops it work
		//--------------------------------------------------------------
		void HandleThreadStop(unsigned int id);

		//--------------------------------------------------------------
		// Method used to get callback from callback container
		//--------------------------------------------------------------
		GCallBack *RipCallBack();

		//--------------------------------------------------------------
		// Method used to set system pivot
		//-------------------------------------------------------------
		void SetPivot(unsigned int pivot) { this->pivot = pivot; }

		//--------------------------------------------------------------
		// Method used to get the number of queued callbacks
		//-------------------------------------------------------------
		unsigned int GetNumberOfQueuedCallbacks() { return this->CallBacks.size(); }

		//--------------------------------------------------------------
		// Method that returns iterator for id in container
		// this method is not safe and should be locked every time
		//-------------------------------------------------------------
		WorkingThreadsContainer::iterator GetWorkingThreadIteratorById(unsigned int id) { return this->WorkingThreads.find(id); }

	private:
		//--------------------------------------------------------------
		// Method used to generate new thread id
		//-------------------------------------------------------------
		unsigned int GenerateId();

		//--------------------------------------------------------------
		// Method used to create a new working Thread
		//--------------------------------------------------------------
		void AddThread(bool perma);

		//--------------------------------------------------------------
		// Static method used by thread to stop temporary thread
		//--------------------------------------------------------------
		static void RemoveThread(void *rstruct, GWorkingThread *execThread);

		void EraseThreadOnIterator(WorkingThreadsContainer::iterator it) { this->WorkingThreads.erase(it); }

		//--------------------------------------------------------------
		// Method used to get the begin iterator of threads container
		// ALWAYS lock the container before using it
		//-------------------------------------------------------------
		WorkingThreadsContainer::iterator GetWorkingThreadsIterator() { return this->WorkingThreads.begin(); }
	}; 
};