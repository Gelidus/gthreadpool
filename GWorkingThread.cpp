/*********************************************
 *	@author - Gelidus (Peter Malina)
 *
 *	Copyright (c) - Gelidus (Peter Malina)
 *	This system can be used in non-profit
 *	applications or for educational purpose 
 *	without contacting author.
 *********************************************/

#include "stdafx.h"

namespace GCallBackLib
{
	GWorkingThread::~GWorkingThread() {
		this->manager = 0;
	}

	void GWorkingThread::WorkingThread(GWorkingThread *thread) {
		thread->SetWorking(true);
		while(thread->ShouldRun()) {

			GCallBack *callback = thread->GetCallBack();
			if(callback != nullptr) {
				thread->SetExecuting(true);
				callback->GetMethod()(callback->GetArgument(), thread);
				thread->ExecuteHandler();
				thread->SetExecuting(false);
				continue;
			}
			std::this_thread::sleep_for(std::chrono::microseconds(15));

			if(!thread->IsPermanent()) {
				if((unsigned int)(clock() - thread->GetLastExecutionTime()) > thread->GetThreadTimeout()) {
					break; // kill thread that is timed out ...
				}
			}
		}
		thread->SetWorking(false);

		if(!thread->IsPermanent()) {
			thread->HandleTemporaryThreadStop();
		}
	}

	void GWorkingThread::Start() {
		this->shouldRun = true;

		this->workingThread = std::thread (GWorkingThread::WorkingThread, this);
	}

	void GWorkingThread::HandleTemporaryThreadStop() {
		this->manager->HandleThreadStop(this->id);
	}

	void GWorkingThread::SetWorking(bool working) {
		this->working = working;
		manager->SetThreadWorking(working);
	}

	GCallBack *GWorkingThread::GetCallBack() {
		return this->manager->RipCallBack();
	}

	void GWorkingThread::SetExecuting(bool exec) { 
		this->executingCallback = exec;
		this->manager->SetThreadExecutingCallBack(exec);
	}

	void GWorkingThread::ExecuteHandler() {
		this->lastExecutionTime = clock();
	} 
}