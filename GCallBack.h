/*********************************************
 *	@author - Gelidus (Peter Malina)
 *
 *	Copyright (c) - Gelidus (Peter Malina)
 *	This system can be used in non-profit
 *	applications or for educational purpose 
 *	without contacting author.
 *********************************************/

namespace GCallBackLib {

	class GCallBack {
	public:

		//-------------------------------------------------------------
		// Priority enum, which is used to determine the priority of callbacks
		// $5 NONE - this element has no priority and will never create thread
		// $4 LOW - lowest priority, this callback will be called as one of last
		// $3 NORMAL - normal priority, default
		// $2 HIGH - high priority, this callback will be called as soon as any 
		//			thread will be avaliable
		// $1 VERY_HIGH - this callback will be called immediately
		//-------------------------------------------------------------
		enum PRIORITY {
			VERY_HIGH = 1,
			HIGH,
			NORMAL,
			LOW,
			NONE,
		};

		//--------------------------------------------------------------
		// Type definition of CallBack function pointer
		// $1 (void*) - any type of parameter (maybe object?)
		//--------------------------------------------------------------
		typedef void (*CallBack)(void*, GWorkingThread*);

		//--------------------------------------------------------------
		// GCallBack class constructor 
		// $1 (*function)(void*) - argument in which is stored the callback
		// $2 (priority) - specifies the priority of the callback, default is normal
		//--------------------------------------------------------------
		GCallBack(CallBack function, GCallBackMgr *manager = nullptr, PRIORITY priority = PRIORITY::NORMAL) {
			this->method = function;
			this->manager = manager;
			this->priority = priority;
			this->argument = 0;
		}

		//--------------------------------------------------------------
		// GCallBack class destructor
		//--------------------------------------------------------------
		~GCallBack();

	private:
		//-------------------------------------------------------------
		// Pointer to manager in which will be callback executed
		//-------------------------------------------------------------
		GCallBackMgr *manager;

		//-------------------------------------------------------------
		// Pointer to function that will be called (private)
		//-------------------------------------------------------------
		CallBack method;

		//-------------------------------------------------------------
		// Time when was the callback called - used for better callback management
		//-------------------------------------------------------------
		clock_t callTime;

		//-------------------------------------------------------------
		// Priority of the callback
		//-------------------------------------------------------------
		PRIORITY priority;

		//-------------------------------------------------------------
		// Argument passed to callback method
		//-------------------------------------------------------------
		void *argument;

	public:

		//--------------------------------------------------------------
		// Get method for the function pointer
		//--------------------------------------------------------------
		CallBack GetMethod() { return this->method; }

		//--------------------------------------------------------------
		// Get method for priority of the callback
		//--------------------------------------------------------------
		PRIORITY GetPriority() { return this->priority; }

		//--------------------------------------------------------------
		// Synchronized CallBack call
		//--------------------------------------------------------------
		virtual void Sync(void *arg);

		//--------------------------------------------------------------
		// Ssynchronized CallBack call handled by system
		//--------------------------------------------------------------
		virtual void ASync(void * arg);

		void *GetArgument() { return this->argument; }

		GCallBackMgr *GetManager() { return this->manager; }
	};
}