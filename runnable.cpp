/*********************************************
 *	@author - Gelidus (Peter Malina)
 *
 *	Copyright (c) - Gelidus (Peter Malina)
 *	This system can be used in non-profit
 *	applications or for educational purpose 
 *	without contacting author.
 *********************************************/

#include "stdafx.h"

using namespace std;
using namespace GCallBackLib;

void DoSomething(void* arg, GWorkingThread *thread) {
	std::cout << "Executed [" << arg << "]" << std::endl;
}

int main(int argc, char *argv[]) {

	GCallBackMgr *instance = new GCallBackMgr();
	instance->Init(4,15,3000);
	instance->SetPivot(30);

	GCallBack *callback = new GCallBack(DoSomething, instance, GCallBack::PRIORITY::LOW);
	for(auto i = 0; i < 100; i++) {
		callback->ASync(NULL);
	}

	instance->BalancePermaThreads(-2);

	int i; cin >> i;

	instance->ForceStop();

	void *clearList[] = {instance, callback};
	ClearObject(clearList);

	return 0;
}

