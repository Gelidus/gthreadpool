/*********************************************
 *	@author - Gelidus (Peter Malina)
 *
 *	Copyright (c) - Gelidus (Peter Malina)
 *	This system can be used in non-profit
 *	applications or for educational purpose 
 *	without contacting author.
 *********************************************/

#include "stdafx.h"

namespace GCallBackLib {

	GCallBack::~GCallBack() {

	}


	//-------------------------------------------------------------
	// Synchronous method body
	//-------------------------------------------------------------
	void GCallBack::Sync(void *arg = nullptr) {
		this->callTime = clock();
		this->GetMethod()(arg, nullptr);
	}

	//-------------------------------------------------------------
	// Ssynchronous method body
	//-------------------------------------------------------------
	void GCallBack::ASync(void *arg = nullptr) {
		this->callTime = clock();
		this->argument = arg;
		if(this->manager != nullptr) {
			this->manager->AddCallBack(this);
		}
	}
}