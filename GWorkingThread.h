/*********************************************
 *	@author - Gelidus (Peter Malina)
 *
 *	Copyright (c) - Gelidus (Peter Malina)
 *	This system can be used in non-profit
 *	applications or for educational purpose 
 *	without contacting author.
 *********************************************/

namespace GCallBackLib
{
	class GCallBackMgr; // no idea why, but it works
	class GCallBack;

	class GWorkingThread final {
	public:
		GWorkingThread(unsigned int id, unsigned int timeout, GCallBackMgr *manager, bool permanent = false) {
			this->id = id;
			this->timeout = timeout;
			this->manager = manager;
			this->working = false;
			this->executingCallback = false;
			this->shouldRun = true;
			this->permanent = permanent;
			this->lastExecutionTime = clock();
		}
		~GWorkingThread();
	private:
		unsigned int id, timeout;

		//-------------------------------------------------------------
		// Specifies if thread is currently working (started) or not
		//-------------------------------------------------------------
		bool working;

		//-------------------------------------------------------------
		// Specifies if thread is currently executing callback
		//-------------------------------------------------------------
		bool executingCallback;

		bool shouldRun;

		//-------------------------------------------------------------
		// Specifies if the thread is permanent - cannot be changed 
		// once the thread is created
		//-------------------------------------------------------------
		bool permanent;

		//-------------------------------------------------------------
		// Pointer to manager in which will be the callback executed
		//-------------------------------------------------------------
		GCallBackMgr *manager;

		//-------------------------------------------------------------
		// Specifies the execution time of last callback executed by
		// the thread
		//-------------------------------------------------------------
		clock_t lastExecutionTime;

		//-------------------------------------------------------------
		// Thread variable
		//-------------------------------------------------------------
		std::thread workingThread;

	public:

		//-------------------------------------------------------------
		// Gette - method used to get working thread Id
		//-------------------------------------------------------------
		unsigned int GetId() { return this->id; }

		//-------------------------------------------------------------
		// Setter - method used to set if thread is working
		//-------------------------------------------------------------
		void SetWorking(bool working);

		//-------------------------------------------------------------
		// Setter - method used to set if thread is executing callback
		//-------------------------------------------------------------
		void SetExecuting(bool exec);

		//-------------------------------------------------------------
		// Getter - returns true if thread is currently working
		//-------------------------------------------------------------
		bool IsWorking() { return this->working; }

		//-------------------------------------------------------------
		// Getter - returns true if thread should run
		//-------------------------------------------------------------
		bool ShouldRun() { return this->shouldRun; }

		bool IsPermanent() { return this->permanent; }

		bool IsExecutingCallback() { return this->executingCallback; }

		void Start();

		void Stop() { this->shouldRun = false; }

		//-------------------------------------------------------------
		// Method using the thread.join() to join thread to main
		//-------------------------------------------------------------
		void Join() { this->workingThread.join(); }

		void HandleTemporaryThreadStop();

		void ExecuteHandler();

		//-------------------------------------------------------------
		// Getter - returns the time of last callback execution
		//-------------------------------------------------------------
		clock_t GetLastExecutionTime() { return this->lastExecutionTime; }

		unsigned int GetThreadTimeout() { return this->timeout; }

		GCallBack *GetCallBack();
	private:

		//-------------------------------------------------------------
		// Method executed as a working thread
		//-------------------------------------------------------------
		static void WorkingThread(GWorkingThread *ThisThread);
	}; 
}